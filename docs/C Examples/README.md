## C Code Examples
---

### max7219 display control via SPI (max7219.c)
![Screenshot](./max7219.webp?raw=true "7 segment display")

### Persistence of Vision display ( pov_trigger.c )
![Screenshot](../pov_display.webp?raw=true "POV display")

Use 8 LEDs on PORTB to write text in thin air!
