<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AppWindow</name>
    <message>
        <location filename="../KuttyPyGUI.py" line="242"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;{err} at line {num} : {detail}&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:red;&quot;&gt;{err} à la ligne {num} : {detail}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="245"/>
        <source>Finished executing user code</source>
        <translation>Exécution du code utilisateur terminée</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="259"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;----------User Code Started-----------&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:green;&quot;&gt;----------Code utilisateur démarré-----------&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="283"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;----------Kill Signal(Doesn&apos;t work yet. Restart the application)-----------&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:red;&quot;&gt;----------Signak d&apos;arrêt (Pas encore fonctionnel. Relancer l&apos;application)-----------&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="492"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;Compile Error: {res}&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:red;&quot;&gt;Erreur de compilation&#xa0;: {res}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="507"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;Generated Hex File&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:green;&quot;&gt;Fichier HEX généré&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="508"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;Finished Compiling: Generated Hex File&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:green;&quot;&gt;Compilation du fichier HEX terminée&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="510"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;Failed to Compile: {err}&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:red;&quot;&gt;Échec de la compilation&#xa0;: {err}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="521"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;Finished upload&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:green;&quot;&gt;Chergement terminé&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="523"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;Failed to upload&lt;/span&gt;</source>
        <translation>&lt;span style=&quot;color:red;&quot;&gt;Échec du chargement&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="527"/>
        <source>Open a hex file to upload to your KuttyPy</source>
        <translation>Ouvrir un fichier HEX à charger dans votre KuttyPy</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="527"/>
        <source>Hex Files (*.hex)</source>
        <translation>Fichier HEX (*.hex)</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="533"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Uploading Code --&lt;/span&gt;&lt;br&gt;</source>
        <translation>&lt;span style=&quot;color:cyan;&quot;&gt;-- Chergement du code --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="539"/>
        <source>Open a C file to edit</source>
        <translation>Ouvrir un fichier C à éditer</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="539"/>
        <source>C Files (*.c *.C)</source>
        <translation>Fichiers C (*.c *.C)</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="544"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Opened File: {f} --&lt;/span&gt;&lt;br&gt;</source>
        <translation>&lt;span style=&quot;color:cyan;&quot;&gt;-- Ouverture du fichier&#xa0;: {f} --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="554"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Compiling and Uploading Code --&lt;/span&gt;&lt;br&gt;</source>
        <translation>&lt;span style=&quot;color:cyan;&quot;&gt;-- Compilation et chargement du code --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="575"/>
        <source>KuttyPy Interactive Console [{0:s}]</source>
        <translation>Console interactive KuttyPy [{0:s}]</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="663"/>
        <source>KuttyPy Interactive Console [ Hardware not detected ]</source>
        <translation>Console interactive KuttyPy [ matériel non détecté]</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="589"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Serial Port Monitor --&lt;/span&gt;&lt;br&gt;</source>
        <translation>&lt;span style=&quot;color:cyan;&quot;&gt;-- Moniteur du port série --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="628"/>
        <source>User App</source>
        <translation>App utilisateur</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="632"/>
        <source>Upload Hex</source>
        <translation>Charger HEX</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="686"/>
        <source>Running</source>
        <translation>En cours de fonctionnement</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="686"/>
        <source>Paused</source>
        <translation>En pause</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="702"/>
        <source>Your title</source>
        <translation>Votre titre</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="703"/>
        <source>some description</source>
        <translation>une description</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="612"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="616"/>
        <source>Save Window as Svg</source>
        <translation>Enregistrer la fenêtre au format SVG</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="637"/>
        <source>Slow</source>
        <translation>Lent</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="637"/>
        <source>Fast</source>
        <translation>Rapide</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="637"/>
        <source>Ultra</source>
        <translation>Ultra</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="656"/>
        <source>Error : Device Disconnected</source>
        <translation>Erreur : périphérique déconnecté</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../utilities/templates/dio_adcConfig.ui" line="14"/>
        <source>ADC Configuration</source>
        <translation>Configuration du convertisseur ADC</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_adcLog.ui" line="139"/>
        <source>0-5000mV Range</source>
        <translation>Calibre 0 à 5000&#xa0;mV</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_adcLog.ui" line="119"/>
        <source>ADMUX</source>
        <translation>ADMUX</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_adcLog.ui" line="60"/>
        <source>Log Read/Write calls</source>
        <translation>Lire journal/Écrire les appels</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_sensor.ui" line="19"/>
        <source>Sensor Reading</source>
        <translation>Lecture de capteur</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_sensor.ui" line="60"/>
        <source>Data Logger</source>
        <translation>Enregistreur de données</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_control.ui" line="14"/>
        <source>Control</source>
        <translation>Contrôle</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_sensor.ui" line="53"/>
        <source>Initialize</source>
        <translation>Initialiser</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_robot.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_sensor.ui" line="83"/>
        <source>Scale</source>
        <translation>Échelle</translation>
    </message>
</context>
<context>
    <name>Frame</name>
    <message>
        <location filename="../utilities/templates/regvals.ui" line="14"/>
        <source>Frame</source>
        <translation>Fenêtre</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="85"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="52"/>
        <source>Pull-Up</source>
        <translation>Pull-Up</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="55"/>
        <source>pullup</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="166"/>
        <source>The GREEN LED attached to PD5 will toggle when the counter crosses this</source>
        <translation>La DEL verte branchée à PD5 changera d&apos;état quand le compteur passe cette valeur</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="169"/>
        <source>PD5â</source>
        <translation>PD5⇆</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="214"/>
        <source>    INPUT</source>
        <translation>    ENTRÉE</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="219"/>
        <source>OUTPUT</source>
        <translation>SORTIE</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_cntr.ui" line="224"/>
        <source>     CNTR</source>
        <translation>     CNTR</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="219"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="152"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="172"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="182"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="192"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="202"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="212"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regedit.ui" line="229"/>
        <source>âREAD</source>
        <translation>↑LIRE</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regvals.ui" line="40"/>
        <source>DDR
-</source>
        <translation>DDR
-</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regvals.ui" line="59"/>
        <source>PORT
-</source>
        <translation>PORT
-</translation>
    </message>
    <message>
        <location filename="../utilities/templates/regvals.ui" line="78"/>
        <source>PIN
-</source>
        <translation>PIN
-</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../utilities/templates/layout.ui" line="14"/>
        <source>KuttyPy Interactive Console</source>
        <translation>Console interactive KuttyPy</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="65"/>
        <source>Playground</source>
        <translation>Zone de travail</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="89"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;img src=&quot;:/control/kpys.jpeg&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;img src=&quot;:/control/kpys.jpeg&quot;/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="126"/>
        <source>Registers</source>
        <translation>Registres</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="170"/>
        <source>Add A New Register</source>
        <translation>Ajouter un nouveau registre</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="181"/>
        <source>Auto-Refresh</source>
        <translation>Auto-rafraîchissement</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="189"/>
        <source>Scripting</source>
        <translation>Coder en Python</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="243"/>
        <source>C Code</source>
        <translation>Coder en C</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="255"/>
        <source>:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="275"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="282"/>
        <source>Compile+Upload</source>
        <translation>Compiler &amp; Charger</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="305"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="345"/>
        <source>Monitor registers being read and set during each operation</source>
        <translation>Les registres du moniteur sont lus et affectés à chaque opération</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="367"/>
        <source>Auto-Clear</source>
        <translation>Auto-nettoyage</translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="377"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="435"/>
        <source>PORT C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="452"/>
        <source>PORT D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="475"/>
        <source>PORT A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../utilities/templates/layout.ui" line="492"/>
        <source>PORT B</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>codeObject</name>
    <message>
        <location filename="../KuttyPyGUI.py" line="242"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;{err} at line {num} : {detail}&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:red;&quot;&gt;{err} à la ligne {num} : {detail}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="245"/>
        <source>Finished executing user code</source>
        <translation type="obsolete">Exécution du code utilisateur terminée</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="259"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;----------User Code Started-----------&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:green;&quot;&gt;----------Code utilisateur démarré-----------&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="283"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;----------Kill Signal(Doesn&apos;t work yet. Restart the application)-----------&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:red;&quot;&gt;----------Signak d&apos;arrêt (Pas encore fonctionnel. Relancer l&apos;application)-----------&lt;/span&gt;</translation>
    </message>
</context>
<context>
    <name>stack</name>
    <message>
        <location filename="../utilities/templates/dio.ui" line="14"/>
        <source>StackedWidget</source>
        <translation>Pile</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio.ui" line="77"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio.ui" line="37"/>
        <source>Pull-Up</source>
        <translation>Pull-Up</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio.ui" line="40"/>
        <source>pullup</source>
        <translation>pullup</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio.ui" line="60"/>
        <source>INPUT</source>
        <translation>ENTRÉE</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio.ui" line="106"/>
        <source>OUTPUT</source>
        <translation>SORTIE</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_adc.ui" line="152"/>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
    <message>
        <location filename="../utilities/templates/dio_pwm.ui" line="146"/>
        <source>PWM</source>
        <translation>PWM</translation>
    </message>
</context>
<context>
    <name>uploadObject</name>
    <message>
        <location filename="../KuttyPyGUI.py" line="492"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;Compile Error: {res}&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:red;&quot;&gt;Erreur de compilation&#xa0;: {res}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="507"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;Generated Hex File&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:green;&quot;&gt;Fichier HEX généré&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="508"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;Finished Compiling: Generated Hex File&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:green;&quot;&gt;Compilation du fichier HEX terminée&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="510"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;Failed to Compile: {err}&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:red;&quot;&gt;Échec de la compilation&#xa0;: {err}&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="521"/>
        <source>&lt;span style=&quot;color:green;&quot;&gt;Finished upload&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:green;&quot;&gt;Chergement terminé&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="523"/>
        <source>&lt;span style=&quot;color:red;&quot;&gt;Failed to upload&lt;/span&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:red;&quot;&gt;Échec du chargement&lt;/span&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="527"/>
        <source>Open a hex file to upload to your KuttyPy</source>
        <translation type="obsolete">Ouvrir un fichier HEX à charger dans votre KuttyPy</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="527"/>
        <source>Hex Files (*.hex)</source>
        <translation type="obsolete">Fichier HEX (*.hex)</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="533"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Uploading Code --&lt;/span&gt;&lt;br&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:cyan;&quot;&gt;-- Chergement du code --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="539"/>
        <source>Open a C file to edit</source>
        <translation type="obsolete">Ouvrir un fichier C à éditer</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="539"/>
        <source>C Files (*.c *.C)</source>
        <translation type="obsolete">Fichiers C (*.c *.C)</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="544"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Opened File: {f} --&lt;/span&gt;&lt;br&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:cyan;&quot;&gt;-- Ouverture du fichier&#xa0;: {f} --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="554"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Compiling and Uploading Code --&lt;/span&gt;&lt;br&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:cyan;&quot;&gt;-- Compilation et chargement du code --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="589"/>
        <source>&lt;span style=&quot;color:cyan;&quot;&gt;-- Serial Port Monitor --&lt;/span&gt;&lt;br&gt;</source>
        <translation type="obsolete">&lt;span style=&quot;color:cyan;&quot;&gt;-- Moniteur du port série --&lt;/span&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="628"/>
        <source>User App</source>
        <translation type="obsolete">App utilisateur</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="632"/>
        <source>Upload Hex</source>
        <translation type="obsolete">Charger HEX</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="686"/>
        <source>Running</source>
        <translation type="obsolete">En cours de fonctionnement</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="686"/>
        <source>Paused</source>
        <translation type="obsolete">En pause</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="702"/>
        <source>Your title</source>
        <translation type="obsolete">Votre titre</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="703"/>
        <source>some description</source>
        <translation type="obsolete">une description</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="575"/>
        <source>KuttyPy Interactive Console [{0:s}]</source>
        <translation type="obsolete">Console interactive KuttyPy [{0:s}]</translation>
    </message>
    <message>
        <location filename="../KuttyPyGUI.py" line="577"/>
        <source>KuttyPy Interactive Console [ Hardware not detected ]</source>
        <translation type="obsolete">Console interactive KuttyPy [ matériel non détecté]</translation>
    </message>
</context>
</TS>
